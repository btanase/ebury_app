import requests

from collections import OrderedDict

from django import forms
from .models import BookedTrade


def get_available_currencies():
    data = requests.get("http://api.fixer.io/latest")
    if data.status_code in [200]:
        all_currencies = sorted(data.json()['rates'].keys())
        options = [("", "")]  # first pos in dropdown needs to be empty string
        options.extend([(item, item) for item in all_currencies])
        return options
    return []

CHOICES = get_available_currencies()


class FunkyForm(object):
    """
    A funky form: add bootstrap classes to form fields.
    """
    def __init__(self, *args, **kwargs):
        super(FunkyForm, self).__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})


class NewTradeForm(FunkyForm, forms.ModelForm):
    sell_ccy = forms.CharField(widget=forms.Select(choices=CHOICES))
    sell_amount = forms.DecimalField()
    buy_ccy = forms.CharField(widget=forms.Select(choices=CHOICES))
    buy_amount = forms.CharField()
    rate = forms.CharField()

    class Meta:
        model = BookedTrade
        fields = ('sell_ccy', 'sell_amount', 'rate', 'buy_ccy', 'buy_amount')

    def __init__(self, *args, **kwargs):
        super(NewTradeForm, self).__init__(*args, **kwargs)
        self.fields['buy_amount'].widget.attrs['readonly'] = True
        self.fields = OrderedDict([
            (name, self.fields[name]) for name in self._meta.fields
        ])
        for name, field in self.fields.items():
            field.widget.attrs.update({'required': 'required'})
