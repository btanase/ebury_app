# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.crypto import get_random_string
from django.db import models


class BookedTrade(models.Model):
    id = models.CharField(max_length=9, primary_key=True)
    sell_ccy = models.CharField(max_length=3)
    sell_amount = models.DecimalField(max_digits=10, decimal_places=2)
    buy_ccy = models.CharField(max_length=3)
    buy_amount = models.DecimalField(max_digits=10, decimal_places=2)
    rate = models.DecimalField(max_digits=7, decimal_places=4)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created']

    def __unicode__(self):
        return str(self.created)

    def generate_custom_id(self):
        return u'{tag}{random_alphanum}'.format(
            tag=u'TR',
            random_alphanum=get_random_string(length=7)
        )

    def save(self, *args, **kwargs):
        self.id = self.generate_custom_id()
        super(BookedTrade, self).save(*args, **kwargs)
