# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import CreateView, ListView
from django.core.urlresolvers import reverse_lazy

from .models import BookedTrade
from .forms import NewTradeForm


class IndexView(ListView):
    template_name = 'base/index.html'
    model = BookedTrade
    http_method_names = [u'get']

    def get_queryset(self, *args, **kwargs):
        return self.model.objects.filter()


class NewTradeView(CreateView):
    http_method_names = [u'post', u'get']
    template_name = 'base/new_trade.html'
    model = BookedTrade
    form_class = NewTradeForm
    success_url = reverse_lazy('index')
